class TestController {
  constructor() {
    this.name = 'test';
    this.items = [
      {
        id: "1",
        name: "item one"
      },
      {
        id: "2",
        name: "item two"
      }
    ];
  }
}

export default TestController;
