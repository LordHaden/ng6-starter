import TestModule from './test'

describe('Test', () => {
  let $rootScope, $state, $location, $componentController, $compile;

  beforeEach(window.module(TestModule));

  beforeEach(inject(($injector) => {
    $rootScope = $injector.get('$rootScope');
    $componentController = $injector.get('$componentController');
    $state = $injector.get('$state');
    $location = $injector.get('$location');
    $compile = $injector.get('$compile');
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
    it('Test component should be visible when navigates to /test', () => {
      $location.url('/test');
      $rootScope.$digest();
      expect($state.current.component).to.eq('test');
    });
  });

  describe('Controller', () => {
    // controller specs
    let controller;
    beforeEach(() => {
      controller = $componentController('test', {
        $scope: $rootScope.$new()
      });
    });

    it('has a name property', () => { // erase if removing this.name from the controller
      expect(controller).to.have.property('name');
    });

    it("has an array of items", () => {
      expect(controller.items).to.be.an.instanceOf(Array);
    });

    it("has first item with keys: id, name", () => {
      expect(controller.items[0]).to.all.have.keys('id', 'name');
    });
  });

  describe('View', () => {
    // view layer specs.
    let scope, template;

    beforeEach(() => {
      scope = $rootScope.$new();
      template = $compile('<test></test>')(scope);
      scope.$apply();
    });

    it('has name in template', () => {
      expect(template.find('h1').html()).to.eq('test');
    });

    it('has item(s) in view', () => {
      expect(template.find('li')).not.to.be.empty;
    });

  });
});
